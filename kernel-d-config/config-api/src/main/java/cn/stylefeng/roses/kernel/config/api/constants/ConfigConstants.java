package cn.stylefeng.roses.kernel.config.api.constants;

/**
 * 系统配置表的常量
 *
 * @author fengshuonan
 * @date 2020/10/16 11:05
 */
public interface ConfigConstants {

    /**
     * config模块的名称
     */
    String CONFIG_MODULE_NAME = "kernel-d-config";

    /**
     * 异常枚举的步进值
     */
    String CONFIG_EXCEPTION_STEP_CODE = "04";

}
