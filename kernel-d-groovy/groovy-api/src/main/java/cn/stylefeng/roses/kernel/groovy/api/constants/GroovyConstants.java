package cn.stylefeng.roses.kernel.groovy.api.constants;

/**
 * groovy常量
 *
 * @author fengshuonan
 * @date 2021/1/22 16:38
 */
public interface GroovyConstants {

    /**
     * groovy模块的名称
     */
    String GROOVY_MODULE_NAME = "kernel-d-groovy";

    /**
     * 异常枚举的步进值
     */
    String GROOVY_EXCEPTION_STEP_CODE = "26";

}
