package cn.stylefeng.roses.kernel.i18n.modular.mapper;

import cn.stylefeng.roses.kernel.i18n.modular.entity.Translation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 多语言表 Mapper 接口
 *
 * @author fengshuonan
 * @date 2021/1/24 19:22
 */
public interface TranslationMapper extends BaseMapper<Translation> {


}
