package cn.stylefeng.roses.kernel.office.api.constants;

/**
 * Office 模块常量
 *
 * @author luojie
 * @date 2020/11/4 10:17
 */
public interface OfficeConstants {

    /**
     * office模块的名称
     */
    String OFFICE_MODULE_NAME = "kernel-d-office";

    /**
     * 异常枚举的步进值
     */
    String OFFICE_EXCEPTION_STEP_CODE = "19";

    /**
     * Excel导出时默认Sheet的名称
     */
    String OFFICE_EXCEL_DEFAULT_SHEET_NAME = "Sheet";

    /**
     * Excel导出时默认文件名称
     */
    String OFFICE_EXCEL_EXPORT_DEFAULT_FILE_NAME = "export";

}
