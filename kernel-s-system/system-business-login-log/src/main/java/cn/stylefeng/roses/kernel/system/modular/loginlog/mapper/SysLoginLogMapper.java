package cn.stylefeng.roses.kernel.system.modular.loginlog.mapper;

import cn.stylefeng.roses.kernel.system.modular.loginlog.entity.SysLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统应用mapper接口
 *
 * @author fengshuonan
 * @date 2020/3/13 16:17
 */
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {
}
