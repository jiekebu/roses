package cn.stylefeng.roses.kernel.system.modular.notice.mapper;

import cn.stylefeng.roses.kernel.system.modular.notice.entity.SysNotice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 通知表 Mapper 接口
 *
 * @author liuhanqing
 * @date 2021/1/8 22:45
 */
public interface SysNoticeMapper extends BaseMapper<SysNotice> {

}
