package cn.stylefeng.roses.kernel.system.modular.resource.mapper;

import cn.stylefeng.roses.kernel.system.modular.resource.entity.SysResource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 资源表 Mapper 接口
 *
 * @author fengshuonan
 * @date 2020/11/23 22:45
 */
public interface SysResourceMapper extends BaseMapper<SysResource> {

}
