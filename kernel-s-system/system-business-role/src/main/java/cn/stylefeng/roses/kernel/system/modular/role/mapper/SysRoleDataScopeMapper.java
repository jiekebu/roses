package cn.stylefeng.roses.kernel.system.modular.role.mapper;

import cn.stylefeng.roses.kernel.system.modular.role.entity.SysRoleDataScope;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统角色范围mapper接口
 *
 * @author majianguo
 * @date 2020/11/5 下午4:15
 */
public interface SysRoleDataScopeMapper extends BaseMapper<SysRoleDataScope> {
}
