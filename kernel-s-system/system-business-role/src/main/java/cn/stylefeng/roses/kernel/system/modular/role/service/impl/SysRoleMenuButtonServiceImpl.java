package cn.stylefeng.roses.kernel.system.modular.role.service.impl;

import cn.stylefeng.roses.kernel.system.modular.role.entity.SysRoleMenuButton;
import cn.stylefeng.roses.kernel.system.modular.role.mapper.SysRoleMenuButtonMapper;
import cn.stylefeng.roses.kernel.system.modular.role.service.SysRoleMenuButtonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 角色按钮关联 服务实现类
 *
 * @author fengshuonan
 * @date 2021/01/09 11:48
 */
@Service
public class SysRoleMenuButtonServiceImpl extends ServiceImpl<SysRoleMenuButtonMapper, SysRoleMenuButton> implements SysRoleMenuButtonService {

}
