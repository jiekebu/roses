package cn.stylefeng.roses.kernel.system.modular.role.service.impl;

import cn.stylefeng.roses.kernel.system.modular.role.entity.SysRoleMenu;
import cn.stylefeng.roses.kernel.system.modular.role.mapper.SysRoleMenuMapper;
import cn.stylefeng.roses.kernel.system.modular.role.service.SysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 角色菜单关联信息
 *
 * @author fengshuonan
 * @date 2020/12/19 18:21
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

}
