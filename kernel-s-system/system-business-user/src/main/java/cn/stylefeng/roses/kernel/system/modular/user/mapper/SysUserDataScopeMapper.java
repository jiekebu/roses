package cn.stylefeng.roses.kernel.system.modular.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.stylefeng.roses.kernel.system.modular.user.entity.SysUserDataScope;

/**
 * 系统用户数据范围mapper接口
 *
 * @author luojie
 * @date 2020/11/6 14:50
 */
public interface SysUserDataScopeMapper extends BaseMapper<SysUserDataScope> {

}
